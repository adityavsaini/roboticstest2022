package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

public class AutoSpin extends CommandBase {

    DriveTrain dTrain;
    double leftSpeed;

    public AutoSpin(DriveTrain drivetrain, double speed){
        dTrain = drivetrain;
        leftSpeed = speed;
        addRequirements(dTrain);
    }

    @Override
    public void execute(){
        dTrain.drive(0, leftSpeed);
    }

    @Override 
    public void end(boolean interrupted){
        dTrain.stopMotors();
    }
    
}
