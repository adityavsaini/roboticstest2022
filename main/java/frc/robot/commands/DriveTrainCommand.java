package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

public class DriveTrainCommand extends CommandBase{

    DriveTrain driveTrain;
    double x, y;

    public DriveTrainCommand(DriveTrain dTrain, double a, double b){
         x = a;
         y = b;
        driveTrain = dTrain;
        addRequirements(driveTrain);
    }

    @Override
    public void execute(){
        driveTrain.drive(x, y);
    }

    @Override
    public void end(boolean interrupted){
        driveTrain.stopMotors();
    }

    @Override
    public boolean isFinished() {
      return false;
    }
  
    
}
