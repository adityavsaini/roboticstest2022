package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.motorcontrol.PWMSparkMax;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;

public class DriveTrain extends SubsystemBase{

    final private DifferentialDrive dDrive;
    private MotorController m_left, m_right;

    public DriveTrain(){
         m_left = new PWMSparkMax(Constants.mLeft);
         m_right = new PWMSparkMax(Constants.mRight);
        m_right.setInverted(true);
        dDrive = new DifferentialDrive(m_left, m_right);
    }

    public void drive(double speedX, double speedY){
        dDrive.tankDrive(speedX, speedY);
    }

    public void stopMotors(){
        dDrive.stopMotor();
    }
    
}
